@extends('client.template')

@section('title')
    صفحه اصلی
@endsection
@section('title')
    پیش دبستانی و دبستان غیر دولتی جلوه با رویکرد قرآنی

@endsection
@section('keywords')
    پیش دبستانی و دبستان غیر دولتی جلوه با رویکرد قرآنی
    مشهد قاسم آباد آموزش و پروش ناحیه 7

@endsection
@section('content')

    <!-- Header -->
    <!-- banner -->
    <header id="head">

        <div class="fluid_container">
            <div class="" id="camera_wrap_4">
                @foreach($banner as $item)
                    <div data-thumb="{{$item->link}}{{$item->name}}" data-src="{{$item->link}}{{$item->name}}">
                    </div>
                @endforeach
            </div><!-- #camera_wrap_3 -->
        </div><!-- .fluid_container -->
    </header>
    <!-- /Header -->


    <section class="features">
        <div class="row">
            @foreach($event as $item)
                <div class="col-md-4">

                    <div class="grey-box-icon b{{$item->id}}">
                        <h1>{{$item->subject}}</h1>
                        <p style="direction:rtl">{{$item->description}}</p>
                        <p><a href="{{$item->link}}"><em>لينك</em></a></p>
                    </div><!--grey box -->
                </div><!--/span3-->
            @endforeach

        </div>
    </section>
    <section class="news-box top-margin">
        <h2><span>ویدئو</span></h2>
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-12" style="textalign:center">
                <div class="newsBox">
                    <div class="thumbnail">
                        <div class="caption maxheight2">
                            <div class="box_inner">
                                <div class="box">
                                    <video width="320" height="240" controls>
                                        <source
                                                src="{{url('/video/1.mp4')}}"
                                                type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="newsBox">
                    <div class="thumbnail">
                        <div class="caption maxheight2">
                            <div class="box_inner">
                                <div class="box">
                                    <video width="320" height="240" controls>
                                        <source
                                                src="{{url('/video/2.mp4')}}"
                                                type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="newsBox">
                    <div class="thumbnail">
                        <div class="caption maxheight2">
                            <div class="box_inner">
                                <video width="320" height="240" controls>
                                    <source src="{{url('/video/3.mp4')}}" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>



    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-box clearfix "><h2 title="About" class="title-box_primary">معرفی آموزشگاه</h2></div>
                <div class="glyphicon-text-size jus" style="line-height: 2em; font-size: larger;text-align: justify">
                    <p class="" style="text-align: justify ">
                        پیش دبستانی و دبستان جلوه معراج با از سال 1394 در حال فعالیت می باشد. این مجموعه با بهره گیری از
                        همکاران جوان دارای سابقه کاری و خلاق و مدیریت هدفمند دارای فعالیت های زیادی با توجه به نیاز روز
                        کودکان و در
                        راستای افزایش خلاقیت آن هاست.
                    <p>
                        انجام بازی ها، سرگرمی ها و چالش های گروهی برای آشنایی با محیط های کاری گروهی و فعالیت های گروه
                        محور،انجام
                        بازی های حرکتی در اتاق بازی، حضور برنامه ریزی شده در برنامه های ورزشی متنوع برای تقویت استعداد
                        ها و کشف
                        توانمندی های ورزشی کودکان در سالن سرپوشیده ورزشی، بررسی منظم و زمان بندی شده سلامت کودکان با
                        چکاپ های دوره
                        ای، دعوت برای حضور کارشناسان در حوزه های مختلف ایمنی، بهداشت، مشاغل و صنایع برای حضور در برنامه
                        های این
                        مجموعه برای آشنایی کودکان با مشاغل مختلف و فراگیری مهارت های لازم در مواجه به موقعیت های گوناگون
                        <a href="/aboutus" title="read more" class="btn-inline " target="_self">ادامه مطب ...</a></div>

                </p>
            </div>
        </div>
    </section>

@endsection
