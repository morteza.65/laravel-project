<!doctype html>
<html>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-168079019-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-168079019-1');
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    @yield('data')

    <title>
        جلوه معراج -
        @yield('title')
    </title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Morteza Aghdasi">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">


    <meta property="og:title" content="جلوه معراج"/>
    <meta property="og:url" content="@yield('url')"/>
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="Jelveh Meraj"/>
    <meta property="og:description" content="@yield('description')"/>
    <meta property="og:image" content="{{ url('/images/logo.png') }}"/>
    <meta name="msapplication-TileImage" content="{{ url('/images/logo.png') }}">
    <link rel="shortcut icon" type="image/png" href="{{ url('/images/logo.png') }}"/>

    <meta property="og:image" content="{{ url('') }}"/>
    <meta name="robots" content="index, follow"/>


    <!--gallery-->
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- js -->
    <link rel="stylesheet" href="{{url('/assets/css/flexslider.css')}}" type="text/css" media="screen"/>
    <script src="{{url('/assets/js/jquery-1.11.1.min.js')}}"></script>
    <!-- //js -->
    <link rel="stylesheet" href="{{url('/assets/css/chocolat.css')}}" type="text/css" media="screen" charset="utf-8">
    <!-- for-gallery-rotation -->
    <script src="{{url('/assets/js/modernizr.custom.97074.js')}}"></script>
    <!-- //for-gallery-rotation -->
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Peralta' rel='stylesheet' type='text/css'>
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="{{url('/assets/js/move-top.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/easing.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>

    @yield('style')

    <link rel="favicon" href="{{url('/assets/images/favicon.png')}}">
    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap-theme.css')}}" media="screen">
    <link rel="stylesheet" href="{{url('/assets/css/style.css')}}">
    <link rel='stylesheet' id='camera-css' href='{{url('/assets/css/camera.css')}}' type='text/css' media='all'>
    <link rel="stylesheet" href="{{url('/assets/css/main.css')}}">
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap.rtl.min.css')}}">


</head>
<body>
<div class="container">
    @include('include.header')
    @yield('content')
    @include('include.footer')
</div>
<!-- JavaScript libs are placed at the end of the document so the pages load faster -->

<script src="{{url('/persian-datepicker/jquery-1.12.0.min.js')}}"></script>
<script src="{{url('/persian-datepicker/persian-date.js')}}"></script>
<script src="{{url('/persian-datepicker/persian-datepicker-0.4.5.js')}}"></script>
<script src="{{url('/assets/js/modernizr-latest.js')}}"></script>
@yield('script')

<script type='text/javascript' src='{{url('/assets/js/jquery.min.js')}}'></script>
<script type='text/javascript' src='{{url('/assets/js/fancybox/jquery.fancybox.pack.js')}}'></script>

<script type='text/javascript' src='{{url('/assets/js/jquery.mobile.customized.min.js')}}'></script>
<script type='text/javascript' src='{{url('/assets/js/jquery.easing.1.3.js')}}'></script>
<script type='text/javascript' src='{{url('/assets/js/camera.min.js')}}'></script>
<script src='{{url('/assets/js/bootstrap.js')}}'></script>


<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('/assets/js/custom.js')}}"></script>


<script>
    jQuery(function () {

        jQuery('#camera_wrap_4').camera({
            transPeriod: 500,
            time: 3000,
            height: '600',
            loader: 'false',
            pagination: true,
            thumbnails: false,
            hover: false,
            playPause: false,
            navigation: false,
            opacityOnGrid: false,
            imagePath: 'assets/images/'
        });

    });

</script>
</body>

</html>
