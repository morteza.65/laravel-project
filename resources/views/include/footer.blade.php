<!--footer-->
<div class="footer-w3">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-6 footer-grid">
                <h4>نمایش مشخصات</h4>
                <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3213.1147175565925!2d59.47478398590277!3d36.358000100167835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f6c8cd4b12f80a1%3A0xd6a47d072a0d9b84!2z2K_YqNiz2KrYp9mGINis2YTZiNmHINmF2LnYsdin2Kw!5e0!3m2!1sfa!2s!4v1589213201853!5m2!1sfa!2s"
                        width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                        tabindex="0"></iframe>
            </div>
            <div class="col-md-6 footer-grid" style="padding-top:100px ">
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>مشهد شهرک غرب جاهد شهر بوستان
                        18.6 پیش دبستانی و دبستان پسرانه جلوه معراج
                    </li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> 5135130765 +98</li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> 5135130542 +98</li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> 9151258904 +98</li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a
                                href="mailto:example@mail.com"> example@mail.com</a></li>
                    <li><i class="glyphicon glyphicon-time" aria-hidden="true"></i>ساعت کاری دفتر7الی14</li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--footer-->
<!---copy--->
<div class="copy-section">
    <div class="container">
        <div class="social-icons">
            <a href="#"><i class="icon1"></i></a>
            <a href="#"><i class="icon2"></i></a>
            <a href="#"><i class="icon3"></i></a>
            <a href="#"><i class="icon4"></i></a>
        </div>
        <div class="copy">
            <p>&copy; کلیه حقوق مادی و معنوی برای دبستان جلوه معراج محفوظ می باشد . هر گونه کپی برداری از محتوای آموزشی
                با ذکر منبع مجاز می باشد.</p>
        </div>
    </div>
</div>
<!---copy--->
