<!-- Fixed navbar -->
<div class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <a class="text-center" href="{{ url('/') }}">
                <h1 style="line-height: normal; color:#425fca">پیش دبستانی و دبستان<br> هوش برتران<br> "جلوه معراج"</h1></a>
        </div>
        <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav pull-right mainNav">


                <li class="c6 dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">سایر <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="sidebar-right.html">Right Sidebar</a></li>
                        <li><a href="#">Dummy Link1</a></li>
                        <li><a href="#">Dummy Link2</a></li>
                        <li><a href="#">Dummy Link3</a></li>
                    </ul>
                </li>


            </ul>


        </div>

        <!--/.nav-collapse -->
    </div>
    <div class="text-center">
        <a class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" role="button">ورود دانش آموزان</a>


        <a class="btn btn-success" role="button" href="/register">پیش ثبت نام</a>


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="modal-title " id="exampleModalLabel">فرم ورود دانش آموزان </p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <header class="text-center">
                            <div class="square"></div>

                        </header>
                        <form action="/" class="templatemo-login-form">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control text-center" placeholder="نام کاربری">
                                    <div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" class="form-control text-center" placeholder="رمز عبور">
                                    <div class="input-group-addon"><i class="fa fa-key fa-fw"></i></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox squaredTwo">
                                    <input type="checkbox" id="c1" name="cc">
                                    <label for="c1"><span></span>مرا به خاطر بسپار</label>
                                </div>
                            </div>

                        </form>


                    </div>
                    <div class="modal-footer " style="text-align: center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                        <button type="button" class="btn btn-primary">وارد شوید</button>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <br>
</div>
<!-- /.navbar -->


