<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Event;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $event= Event::all();
        $banner= Banner::all();
        return view('client.index')->with([
            'event' => $event,
            'banner' => $banner,
        ]);
    }

}
